package Task1Sort;

public interface Sortable {
    int[] sort(int[] array);
}
