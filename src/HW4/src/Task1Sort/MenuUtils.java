package Task1Sort;

import java.util.Arrays;
import java.util.Scanner;

public class MenuUtils {
    static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите массив чисел через пробел");
        String line = scanner.nextLine();
        String[] array = line.split(" +");
        int[] arrayInt = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            arrayInt[i] = Integer.parseInt(array[i]);
        }
        return arrayInt;
    }

    static Sortable inputTypeSort() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите тип сортировки: \n 1-Пузырьком вверх \n 2-Пузырьком вниз \n 3-Выбором вниз \n 4-Выбором вверх");
        int typeSort = scanner.nextInt();
        Sortable sort;
        switch (typeSort) {
            case 1:
                sort = new BubbleUp();
                break;
            case 2:
                sort = new BubbleDown();
                break;
            case 3:
                sort = new SelectionDown();
                break;
            case 4:
                sort = new SelectionUp();
                break;
            default:
                System.out.println("Неверный выбор, принимаем Пузырьком вверх");
                sort = new BubbleUp();

        }
        return sort;

    }

    static void print(int[] array) {
        System.out.println(Arrays.toString(array));
    }

    static void print1(int[] A) {
        System.out.println(Arrays.toString(A));

    }

}
