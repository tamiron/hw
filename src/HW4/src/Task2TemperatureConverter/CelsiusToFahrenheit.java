package Task2TemperatureConverter;

public class CelsiusToFahrenheit implements Convert{


    @Override
    public double convert(double celsium) {
        double TC = celsium;
        double  TF = (9.0 / 5.0) * TC + 32.0;

        return TF;
    }
}