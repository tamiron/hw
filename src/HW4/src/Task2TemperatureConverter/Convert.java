package Task2TemperatureConverter;

public interface Convert {
    double convert (double celsium);
}
