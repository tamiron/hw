package Task2TemperatureConverter;

public class CelsiusToKelvin implements Convert {

    @Override
    public double convert(double celsium) {
        double TC = celsium;
        double  TK = TC + 273.15;

        return TK;
    }


}
