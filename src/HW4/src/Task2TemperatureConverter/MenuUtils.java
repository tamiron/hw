package Task2TemperatureConverter;

import Task3Printout.LaserPrinter;

import java.util.Arrays;
import java.util.Scanner;

public class MenuUtils {
    public static double numberInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите градусы цельсия");
        double celsium = scanner.nextDouble();
        return celsium;
    }

    public static Convert TempConverter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите нужную конвертацию : " +
                "\n 1 -Цельсия в Фаренгейты \n 2 - Цельсия в Кельвины");
        int sortNumber = scanner.nextInt();
        Convert convert;
        switch (sortNumber) {

            case 1:
                convert = new CelsiusToFahrenheit();
                break;
            case 2:
                convert = new CelsiusToKelvin();
                break;

            default:
                System.out.println("Выбор некорректен. Выбираем Цельсий в Кельвин ");
                convert = new CelsiusToKelvin();


        }
        return convert;
    }


}
