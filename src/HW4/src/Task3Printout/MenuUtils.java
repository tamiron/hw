package Task3Printout;

import java.util.Scanner;

public class MenuUtils {
    public static Printer deviceList() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Список доступных устройств :\n 1- 3D Принтер \n 2- Лазерный принтер \n выберите принтер:");
        int sortNumber = scanner.nextInt();
        Printer result;
        switch (sortNumber) {
            case 1:
                result = new Printer3D();
                break;
            case 2:
                result = new LaserPrinter();
                break;
            default:
                System.out.println("Выбор некорректен. Устанавливается принтер по умолчанию (лазерный принтер)");
                result = new LaserPrinter();
        }
        return result;
    }

    public static Printer devictlist1(Printer dev) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберете следующие действие: " +
                "\n 1- Напечатать   новое   сообщение   на   текущем устройстве " +
                "\n 2-Выбрать другое ");
        int sortNumber1 = scanner.nextInt();
        Printer result;
        switch (sortNumber1) {
            case 1:
                result = dev;
                break;
            case 2:
                result = deviceList();
                break;
            default:
                result = dev;
        }
        return result;
    }

    public static boolean isExit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Хотите закочить \n 1- Да \n 2- Нет");
        return scanner.nextInt() == 1;
    }
}
